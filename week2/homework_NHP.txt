Homework:

Q1) shirt

Q2) Create a function

import numpy as np

def average_func(input1,input2,input3,path_to_save_file):
	# Purpose: Calculate the average of three inputs and save as txt file
	# Input: input1, input2, input3 and path to save the txt file
	# Output: The txt file saves three inputs and the average
	input_arr = [input1,input2,input3]
	input_arr = np.asanyarray(input_arr)
	average = np.mean(input_arr,axis=0)
	
	average_file_name = 'test_aver.txt'
	average_file = open(path_to_save_file +'/' + average_file_name,"w")
	
	average_file.writelines('Inputs: ')
	average_file.writelines((str(input_arr[0]), ', ', str(input_arr[1]) , ', ', str(input_arr[2])))
	average_file.writelines('\n')
	average_file.writelines('Average value is: ')
	average_file.writelines((str(average)))
	average_file.close()
	
	return(average_file)
